input {
  pipeline {
    address => sophos_fw
  }
}

filter {
  if [message] =~ 'log_type="' { grok {match => { 'message' => 'log_type="(?<event.type>[^"]*)"' } tag_on_failure => ['parse_event.type_failed'] }}
  if [message] =~ 'log_id=' { grok {match => { 'message' => 'log_id=(?<event.id>[^\s]*)' } tag_on_failure => ['parse_event.id_failed'] }}
  if [message] =~ 'status="' { grok {match => { 'message' => 'status="(?<event.outcome>[^"]*)"' } tag_on_failure => ['parse_event.outcome_failed'] }}
  if [message] =~ 'src_ip=' { grok {match => { 'message' => 'src_ip=(?<source.ip>[^\s]*)' } tag_on_failure => ['parse_source.ip_failed'] }}
  if [message] =~ 'priority=' { grok {match => { 'message' => 'priority=(?<log.level>[^\s]*)' } tag_on_failure => ['parse_log.level_failed'] }}
  if [message] =~ 'src_port=' { grok {match => { 'message' => 'src_port=(?<source.port>[^\s]*)' } tag_on_failure => ['parse_source.port_failed'] }}
  if [message] =~ 'dst_port=' { grok {match => { 'message' => 'dst_port=(?<destination.port>[^\s]*)' } tag_on_failure => ['parse_destination.port_failed'] }}
  if [message] =~ 'protocol="' { grok {match => { 'message' => 'protocol="(?<network.protocol>[^\\"]*)"' } tag_on_failure => ['parse_network.protocol_failed'] }}

  # Fix for Standard Schema - Need to be removed after some days/weeks and update of dashboards and alerts
  if [message] =~ 'log_subtype="' { grok {match => { 'message' => 'log_subtype="(?<event.action>[^"]*)"' } tag_on_failure => ['parse_event.action_failed'] }}
  if [message] =~ 'dst_ip=' { grok {match => { 'message' => 'dst_ip=(?<destination.ip>[^\s]*)' } tag_on_failure => ['parse_destination.ip_failed'] }}
  if [message] =~ 'out_interface="' { grok {match => { 'message' => 'out_interface="(?<observer.egress.interface.name>[^"]*)"' } tag_on_failure => ['parse_observe.egress.interface.name_failed'] }}
  if [message] =~ 'fw_rule_id=' { grok {match => { 'message' => 'fw_rule_id=(?<rule.id>[^\s]*)' } tag_on_failure => ['parse_rule.id_failed'] }}
  if [message] =~ 'signature_id=' { grok {match => { 'message' => 'signature_id=(?<vulnerability.id>[^\s]*)' } tag_on_failure => ['parse_vulnerability.id_failed'] }}
  if [message] =~ 'in_interface="' { grok {match => { 'message' => 'in_interface="(?<observer.ingress.interface.name>[^"]*)"' } tag_on_failure => ['parse_observe.ingress.interface.name_failed'] }}
  if [message] =~ 'user_name="' { grok {match => { 'message' => 'user_name="(?<source.user.name>[^"]*)"' } tag_on_failure => ['parse_source.user.name_failed'] }}
  if [message] =~ 'device_name="' { grok {match => { 'message' => 'device_name="(?<observer.name>[^"]*)"' } tag_on_failure => ['parse_observer.name_failed'] }}
  if [message] =~ 'device_id="?' { grok {match => { 'message' => 'device_id="?(?<observer.serial_number>[^"\s]*)"?' } tag_on_failure => ['parse_observer.serial_number_failed'] }}
  if [message] =~ 'src_mac=' { grok {match => { 'message' => 'src_mac=(?<source.mac>[^\s]*)' } tag_on_failure => ['parse_source.mac_failed'] }}
  if [message] =~ 'device="' { grok {match => { 'message' => 'device="(?<observer.type>[^"]*)"' } tag_on_failure => ['parse_observer.type_failed'] }}
  if [message] =~ 'log_component="' { grok {match => { 'message' => 'log_component="(?<labels.event.subtype>[^"]*)"' } tag_on_failure => ['parse_labels.event.subtype_failed'] }}
  if [message] =~ 'category="' { grok {match => { 'message' => 'category="(?<event.category>[^"]*)"' } tag_on_failure => ['parse_event.category_failed'] }}
  if [message] =~ 'signature_msg="' { grok {match => { 'message' => 'signature_msg="(?<vulnerability.description>[^"]*)"' } tag_on_failure => ['parse_vulnerability.description_failed'] }}
  if [message] =~ 'classification="' { grok {match => { 'message' => 'classification="(?<rule.category>[^"]*)"' } tag_on_failure => ['parse_rule.category_failed'] }}
  if [message] =~ 'reason="' { grok {match => { 'message' => 'reason="(?<event.reason>[^"]*)"' } tag_on_failure => ['parse_event.reason_failed'] }}
  if [message] =~ 'application="' { grok {match => { 'message' => 'application="(?<network.application>[^"]*)"' } tag_on_failure => ['parse_network.application_failed'] }}
  if [message] =~ 'application_name="' { grok {match => { 'message' => 'application_name="(?<network.application>[^"]*)"' } tag_on_failure => ['parse_network.application_failed'] }}
  if [message] =~ 'av_policy_name="' { grok {match => { 'message' => 'av_policy_name="(?<labels.policy.name>[^"]*)"' } tag_on_failure => ['parse_labels.policy.name_failed'] }}
  if [message] =~ 'from_email_address="' { grok {match => { 'message' => 'from_email_address="(?<source.user.email>[^"]*)"' } tag_on_failure => ['parse_source.user.email_failed'] }}
  if [message] =~ 'subject="' { grok {match => { 'message' => 'subject="(?<labels.email.subject>[^"]*)"' } tag_on_failure => ['parse_labels.email.subject_failed'] }}
  if [message] =~ 'to_email_address="' { grok {match => { 'message' => 'to_email_address="(?<destination.user.email>[^"]*)"' } tag_on_failure => ['parse_destination.user.email_failed'] }}
  if [message] =~ 'email_subject="' { grok {match => { 'message' => 'email_subject="(?<labels.email.subject>[^"]*)"' } tag_on_failure => ['parse_labels.email.subject_failed'] }}
  if [message] =~ 'mailsize=' { grok {match => { 'message' => 'mailsize=(?<labels.email.size>[^\s]*)' } tag_on_failure => ['parse_labels.email.size_failed'] }}
  if [message] =~ 'spamaction="' { grok {match => { 'message' => 'spamaction="(?<fw_sophos.spamaction>[^"]*)"' } tag_on_failure => ['parse_fw_sophos.spamaction_failed'] }}
  if [message] =~ 'querystring=' { grok {match => { 'message' => 'querystring=(?<url.query>[^\s]*)' } tag_on_failure => ['parse_url.query_failed'] }}
  if [message] =~ 'bytessent=' { grok {match => { 'message' => 'bytessent=(?<source.bytes>[^\s]*)' } tag_on_failure => ['parse_source.bytes_failed'] }}
  if [message] =~ 'user_gp="' { grok {match => { 'message' => 'user_gp="(?<source.user.group.name>[^"]*)"' } tag_on_failure => ['parse_source.user.group.name_failed'] }}
  if [message] =~ 'sent_bytes=' { grok {match => { 'message' => 'sent_bytes=(?<source.bytes>[^\s]*)' } tag_on_failure => ['parse_source.bytes_failed'] }}
  if [message] =~ 'dst_domainname="' { grok {match => { 'message' => 'dst_domainname="(?<destination.user.domain>[^"]*)"' } tag_on_failure => ['parse_destination.user.domain_failed'] }}
  if [message] =~ 'src_domainname="' { grok {match => { 'message' => 'src_domainname="(?<source.user.domain>[^"]*)"' } tag_on_failure => ['parse_source.user.domain_failed'] }}
  if [message] =~ 'quarantine_reason="' { grok {match => { 'message' => 'quarantine_reason="(?<fw_sophos.quarantine_reason>[^"]*)"' } tag_on_failure => ['parse_fw_sophos.quarantine_reason_failed'] }}
  if [message] =~ 'sent_pkts=' { grok {match => { 'message' => 'sent_pkts=(?<source.packets>[^\s]*)' } tag_on_failure => ['parse_source.packets_failed'] }}
  if [message] =~ 'recv_bytes=' { grok {match => { 'message' => 'recv_bytes=(?<destination.bytes>[^\s]*)' } tag_on_failure => ['parse_destination.bytes_failed'] }}
  if [message] =~ 'recv_pkts=' { grok {match => { 'message' => 'recv_pkts=(?<destination.packets>[^\s]*)' } tag_on_failure => ['parse_destination.packets_failed'] }}
  if [message] =~ 'tran_dst_ip=' { grok {match => { 'message' => 'tran_dst_ip=(?<destination.nat.ip>[^\s]*)' } tag_on_failure => ['parse_destination.nat.ip_failed'] }}
  if [message] =~ 'tran_src_ip=' { grok {match => { 'message' => 'tran_src_ip=(?<source.nat.ip>[^\s]*)' } tag_on_failure => ['parse_source.nat.ip_failed'] }}
  if [message] =~ 'dstzone="' { grok {match => { 'message' => 'dstzone="(?<observer.egress.zone>[^"]*)"' } tag_on_failure => ['parse_observer.egress.zone_failed'] }}
  if [message] =~ 'tran_src_port=' { grok {match => { 'message' => 'tran_src_port=(?<source.nat.port>[^\s]*)' } tag_on_failure => ['parse_source.nat.port_failed'] }}
  if [message] =~ 'srczone="' { grok {match => { 'message' => 'srczone="(?<observer.ingress.zone>[^"]*)"' } tag_on_failure => ['parse_observer.ingress.zone_failed'] }}
  if [message] =~ 'url="' { grok {match => { 'message' => 'url="?(?<url.original>[^"]*)"?' } tag_on_failure => ['parse_url.original_failed'] }}
  if [message] =~ 'tran_dst_port=' { grok {match => { 'message' => 'tran_dst_port=(?<destination.nat.port>[^\s]*)' } tag_on_failure => ['parse_destination.nat.port_failed'] }}
  if [message] =~ 'category_type="' { grok {match => { 'message' => 'category_type="(?<labels.category.type>[^"]*)"' } tag_on_failure => ['parse_labels.category.type_failed'] }}
  if [message] =~ 'auth_mechanism="' { grok {match => { 'message' => 'auth_mechanism="(?<labels.authentication.method.name>[^"]*)"' } tag_on_failure => ['parse_labels.authentication.method.name_failed'] }}
  if [message] =~ 'sourceip=' { grok {match => { 'message' => 'sourceip=(?<source.ip>[^\s]*)' } tag_on_failure => ['parse_source.ip_failed'] }}
  if [message] =~ 'contenttype="' { grok {match => { 'message' => 'contenttype="(?<http.request.mime_type>[^"]*)"' } tag_on_failure => ['parse_http.request.mime_type_failed'] }}
  if [message] =~ 'domain="' { grok {match => { 'message' => 'domain="(?<url.domain>[^"]*)"' } tag_on_failure => ['parse_url.domain_failed'] }}
  if [message] =~ 'status_code="' { grok {match => { 'message' => 'status_code="(?<http.response.status_code>[^"]*)"' } tag_on_failure => ['parse_http.response.status_code_failed'] }}
  if [message] =~ 'download_file_type="' { grok {match => { 'message' => 'download_file_type="(?<file.type>[^"]*)"' } tag_on_failure => ['parse_file.type_failed'] }}
  if [message] =~ 'upload_file_type="' { grok {match => { 'message' => 'upload_file_type="(?<file.type>[^"]*)"' } tag_on_failure => ['parse_file.type_failed'] }}
  if [message] =~ 'download_file_name="' { grok {match => { 'message' => 'download_file_name="(?<file.name>[^"]*)"' } tag_on_failure => ['parse_file.name_failed'] }}
  if [message] =~ 'upload_file_name="' { grok {match => { 'message' => 'upload_file_name="(?<file.name>[^"]*)"' } tag_on_failure => ['parse_file.name_failed'] }}
  if [message] =~ 'httpstatus="' { grok {match => { 'message' => 'httpstatus="(?<http.response.status_code>[^"]*)"' } tag_on_failure => ['parse_http.response.status_code_failed'] }}
  if [message] =~ 'useragent="' { grok {match => { 'message' => 'useragent="(?<user_agent.original>[^"]*)"' } tag_on_failure => ['parse_user_agent.original_failed'] }}  if [message] =~ 'method=' { grok {match => { 'message' => 'method=(?<app_method>[^\s]*)' } tag_on_failure => ['parse_app_method_failed'] }}
  if [message] =~ 'bytesrcv=' { grok {match => { 'message' => 'bytesrcv=(?<destination.bytes>[^\s]*)' } tag_on_failure => ['parse_destination.bytes_failed'] }}
  if [message] =~ 'usergroupname="' { grok {match => { 'message' => 'usergroupname="(?<source.user.group.name>[^"]*)"' } tag_on_failure => ['parse_source.user.group.name_failed'] }}
  if [message] =~ 'auth_client="' { grok {match => { 'message' => 'auth_client="(?<labels.authentication.client>[^"]*)"' } tag_on_failure => ['parse_labels.authentication.client_failed'] }}
  if [message] =~ 'remoteinterfaceip=' { grok {match => { 'message' => 'remoteinterfaceip=(?<labels.observer.egress.interface.ip>[^\s]*)' } tag_on_failure => ['parse_labels.observer.egress.interface.ip_failed'] }}
  if [message] =~ 'localinterfaceip=' { grok {match => { 'message' => 'localinterfaceip=(?<labels.observer.ingress.interface.ip>[^\s]*)' } tag_on_failure => ['parse_labels.observer.ingress.interface.ip_failed'] }}
  if [message] =~ 'localnetwork="' { grok {match => { 'message' => 'localnetwork="(?<fw_sophos.localnetwork>[^"]*)"' } tag_on_failure => ['parse_fw_sophos.localnetwork_failed'] }}
  if [message] =~ 'client_physical_address="' { grok {match => { 'message' => 'client_physical_address="(?<client.mac>[^"]*)"' } tag_on_failure => ['parse_client.mac_failed'] }}
  if [message] =~ 'remotenetwork="' { grok {match => { 'message' => 'remotenetwork="(?<fw_sophos.remotenetwork>[^"]*)"' } tag_on_failure => ['parse_fw_sophos.remotenetwork_failed'] }}
  if [message] =~ 'localip=' { grok {match => { 'message' => 'localip=(?<host.ip>[^\s]*)' } tag_on_failure => ['parse_host.ip_failed'] }}
  if [message] =~ 'connectionname="' { grok {match => { 'message' => 'connectionname="(?<network.name>[^"]*)"' } tag_on_failure => ['parse_network.name_failed'] }}
  if [message] =~ 'virus="' { grok {match => { 'message' => 'virus="(?<vulnerability.id>[^"]*)"' } tag_on_failure => ['parse_vulnerability.id_failed'] }}
  if [message] =~ 'ftpcommand="' { grok {match => { 'message' => 'ftpcommand="(?<labels.command>[^"]*)"' } tag_on_failure => ['parse_labels.command_failed'] }}
  if [message] =~ 'FTP_direction="' { grok {match => { 'message' => 'FTP_direction="(?<network.direction>[^"]*)"' } tag_on_failure => ['parse_network.direction_failed'] }}
  if [message] =~ 'file_size=' { grok {match => { 'message' => 'file_size=(?<file.size>[^\s]*)' } tag_on_failure => ['parse_file.size_failed'] }}
  if [message] =~ 'file_path="' { grok {match => { 'message' => 'file_path="(?<file.path>[^"]*)"' } tag_on_failure => ['parse_file.path_failed'] }}
  if [message] =~ 'filename="' { grok {match => { 'message' => 'filename="(?<file.name>[^"]*)"' } tag_on_failure => ['parse_file.name_failed'] }}
  if [message] =~ 'ep_health=' { grok {match => { 'message' => 'ep_health=(?<labels.host.health>[^\s]*)' } tag_on_failure => ['parse_labels.host.health_failed'] }}
  if [message] =~ 'ep_uuid=' { grok {match => { 'message' => 'ep_uuid=(?<host.id>[^\s]*)' } tag_on_failure => ['parse_host.id_failed'] }}
  if [message] =~ 'ep_name=' { grok {match => { 'message' => 'ep_name=(?<host.name>[^\s]*)' } tag_on_failure => ['parse_host.name_failed'] }}
  if [message] =~ 'ep_ip=' { grok {match => { 'message' => 'ep_ip=(?<host.ip>[^\s]*)' } tag_on_failure => ['parse_host.ip_failed'] }}
  if [message] =~ 'RED=' { grok {match => { 'message' => 'RED=(?<fw_sophos.red>[^\s]*)' } tag_on_failure => ['parse_fw_sophos.red_failed'] }}
  if [message] =~ 'GREEN=' { grok {match => { 'message' => 'GREEN=(?<fw_sophos.green>[^\s]*)' } tag_on_failure => ['parse_fw_sophos.green_failed'] }}
  if [message] =~ 'YELLOW=' { grok {match => { 'message' => 'YELLOW=(?<fw_sophos.yellow>[^\s]*)' } tag_on_failure => ['parse_fw_sophos.yellow_failed'] }}
  if [message] =~ 'TOTAL=' { grok {match => { 'message' => 'TOTAL=(?<fw_sophos.total>[^\s]*)' } tag_on_failure => ['parse_fw_sophos.total_failed'] }}

  if [message] =~ 'message="' { grok {match => { 'message' => 'message="(?<labels.event.msg>[^"]*)"' } tag_on_failure => ['parse_labels.event.msg_failed'] }}

  # Did not parse the timestamp from sophos logs because the timezone is not in joda canonical id http://joda-time.sourceforge.net/timezones.html
}


output {
  pipeline {
    send_to => normalization
  }
}	
